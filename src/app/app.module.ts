import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ListBooksComponent } from './components/list-books/list-books.component';
import { BookComponent } from './components/book.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBooksComponent,
    BookComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
